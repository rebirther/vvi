<div class="poorvis">
	<a href="#" id="poor_vision" class="poor_vision_toggle">Версия для слабовидящих</a>
	<script>
		if (Cookies.get('poor_vision_toggle')) {
			document.getElementById('poor_vision').innerHTML = "Обычная версия";
		}else{
			document.getElementById('poor_vision').innerHTML = "Версия для слабовидящих";
		}
	</script>
</div>
