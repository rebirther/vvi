<?
/*
 * poor_vision_toggle - cookie, содержит строковый тип данных
 * 
 */

$path = "/theme/custom"; // директория с плагином относительно корня сайта
echo '<script type="text/javascript" src="'.$path.'/poor/js-cookie.js"></script>'; // js-cookie
echo '<script type="text/javascript" src="'.$path.'/poor/poor.js"></script>'; // главный скрипт

// Обработчик
if(isset($_COOKIE["poor_vision_toggle"])) {
	echo '<link id="poor-local" rel="stylesheet" type="text/css" href="'.$path.'/poor/local.css" />'; // стили по умолчанию
	echo '<link id="poor-block" rel="stylesheet" type="text/css" href="'.$path.'/poor/block.css" />'; // стили для панели
	$cookStr = $_COOKIE["poor_vision_toggle"]; // получаем текущий массив значений
	$cookStr = substr($cookStr,2);
	$cookArr = explode("|",$cookStr);
	$propCount = count($cookArr);
	if ($cookArr[0] != null) {
		for ($i = 0; $i < $propCount; $i++) {
			$prop = explode("-",$cookArr[$i]);
			echo  '<link id="poor-'.$prop[0].'" rel="stylesheet" type="text/css" href="'.$path.'/poor/css/'.$cookArr[$i].'.css" />';
		}
	}
}
?>
