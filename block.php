<div id="poor-block" class="poor-block">
	<div class="wrapper">
		<div class="poor-name">Цвет:</div>
  		<div id="color-white" class="color color-white">Ц</div>
  		<div id="color-black" class="color color-black">Ц</div>
  		<div class="poor-name">Шрифт:</div>
  		<div id="font-small" class="font font-small">A</div>  
		<div id="font-medium" class="font font-medium">A</div>
		<div id="font-large" class="font font-large">A</div>
		<div class="poor-name">Интервал:</div>
		<div class="poor-int-name">строчный:</div>
		<div id="line-one" class="line line-one">1.7</div>
		<div id="line-two" class="line line-two">2</div>
		<div class="poor-int-name">символьный:</div>
		<div id="char-one" class="char char-one">1</div>  
		<div id="char-two" class="char char-two">2</div>
		<div class="poor-name">Изображения:</div>
		<div id="image-on" class="image image-on">ВКЛ</div>  
		<div id="image-off" class="image image-off">ВЫКЛ</div>
		<div id="image-gamma" class="image image-gamma">Ч/Б</div>
		<div id="reset-poor" class="image reset-poor">X</div>
	</div>
</div>
