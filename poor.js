// File must have to js-cookie.js

var path = "/bitrix/templates/gornoaltaysk_ru"; // директория до папки со скриптом

// массив содержит все управляемые id данного скрипта
var propArr = [ "color-white", "color-black", // цвет текста
				"font-small", "font-medium", "font-large", // размер шрифта
				"line-one", "line-two", // междустрочный интервал
				"char-one",	"char-two",	// междусимвольный интервал
				"image-on",	"image-off","image-gamma", // наличие изображений
				"reset-poor" ]; // сброс всех параметров 

var cookArr; // Массив с получеными cookies вида: ["color-white",...]
var propAss = {}; // хранит свойства во время изменений
var currId; // Текущее нажатое свойство вида color-white
var currPropArr; // currId преобразованная в массив вида [color,white]



/*
* Функция добавляет тег с файлом .css в документ
* На вход принимает id файла и имя в папке /poor
* Пример входа: addCss("poor-local", "css/color-black");
*/
function addCss(id, name) {
	$('head').append('<link id="' + id + '" type="text/css" rel="stylesheet" href="' + path + '/poor/' + name + '.css"/>');	
}

/*
* Функция зачищает файлы .css, добавленные из этого скрипта
* Так же зачищает cookies
* Пример: removeCssCookies(1); - зачистить все настройки CSS из верхнего блока урправления
* Пример: removeCssCookies(2); - зачистить все настройки CSS. Откат к обычной версии сайта
*/
function removeCssCookies (rate) { 
	if (rate == 1) {
		$('#poor-color').remove();
		$('#poor-font').remove();
		$('#poor-line').remove();
		$('#poor-char').remove();
		$('#poor-image').remove();
		Cookies.set('poor_vision_toggle', 1, { path: '/' });
	}
	if (rate > 1) {
		$('#poor-local').remove();
		$('#poor-block').remove();
		Cookies.remove('poor_vision_toggle', { path: '/' });
	}
}

/*
* Функция преобразует массив вида [ "color-white", ... ]
* в объект вида { color: "white", ... }
* Работает с глобальными массивом и объектом
* Возращает "false" в случае если массив не имеет элементов
*/
function arrCookToObjectCook () {
	cookArr = Cookies.get('poor_vision_toggle').slice(2).split("|");
	if (Cookies.get('poor_vision_toggle').length == 1) return false;
	
	for (var i = 0; i < cookArr.length; i++) {
		var propTmp = cookArr[i].split("-");
		propAss[propTmp[0]] = propTmp[1];
	};
	return true;
}

/* 
* Функция переключает режимы версий 
* Если была "Обычная версия", то подгружаем два .css
* и ставим cookies в значение 1
* Если "Версия для слабовидящих", то чистим всё
*/
/*function toogleClick() {
	if (Cookies.get('poor_vision_toggle')) {
		$('#poor_vision').text('Версия для слабовидящих');
		removeCssCookies(2);
	} else {
		Cookies.set('poor_vision_toggle', 1, { path: '/' });
		$('#poor_vision').text('Обычная версия');
		addCss("poor-local","local");
		addCss("poor-block","block");
	}
}*/

$( document ).ready(function() {
	// Toogle click
	$('a.poor_vision_toggle').click(function() {
		if (Cookies.get('poor_vision_toggle')) {
			$('#poor_vision').text('Версия для слабовидящих');
			removeCssCookies(2);
		} else {
			Cookies.set('poor_vision_toggle', 1, { path: '/' });
			$('#poor_vision').text('Обычная версия');
			addCss("poor-local","local");
			addCss("poor-block","block");
		}
	});

	// Handler
	$(document).click(function(event) {
		var propStr,
		cookStrNew,
		propOld;
		
		// получаем свойство и преобразуем в массив
		currId = event.target.id;
		currPropArr = currId.split("-");
		
		// Сброс настроек
		if (currId == "reset-poor") {
			removeCssCookies(1);
			return;
		}
		
		// Если нажата наша кнопка		
		if(jQuery.inArray(currId, propArr) === -1) return;

		if (arrCookToObjectCook()) {
			console.log("FALSE");
			if (propAss[currPropArr[0]]) {
				propOld = currPropArr[0] + "-" + propAss[currPropArr[0]];
				propAss[currPropArr[0]] = currPropArr[1];
				for (var i = 0; i < Object.keys(propAss).length; i++) { 
					if (i==0) {
						propStr = Object.keys(propAss)[i] + "-" + propAss[Object.keys(propAss)[i]]; 
					} else {
						propStr = propStr + "|" + Object.keys(propAss)[i] + "-" + propAss[Object.keys(propAss)[i]];
					}
				}
				cssRemoved = "_" + propOld;
			} else {
				for (var i = 0; i < Object.keys(propAss).length; i++) {
					if (i==0) {
						propStr = Object.keys(propAss)[i] + "-" + propAss[Object.keys(propAss)[i]]; 
					} else {
						propStr = propStr + "|" + Object.keys(propAss)[i] + "-" + propAss[Object.keys(propAss)[i]];
					}
				}
				propStr = propStr + "|" + currId;	
			}
			$('#poor-'+currPropArr[0]).remove();
			addCss("poor-"+currPropArr[0],"css/"+currId);
		} else {
			propStr = currId;
			addCss("poor-"+currPropArr[0],"css/"+currId);
		}
		cookStrNew = "1|" + propStr;
		Cookies.set('poor_vision_toggle', cookStrNew, { path: '/' });

	});
});
